package com.mocha.demokeyboard

import android.view.inputmethod.EditorInfo

/**
 * Mocha IME extension functions.
 */

internal fun EditorInfo?.isChanged(other: EditorInfo?): Boolean {
    return this?.packageName != other?.packageName || this?.fieldId != other?.fieldId
}