package com.mocha.demokeyboard

import androidx.lifecycle.Observer
import com.mocha.sdk.*

internal class SearchResultsController(private val callback: (showWhat: ShowWhat) -> Unit) {

    internal enum class ShowWhat {
        BRANDS,
        PRODUCTS
    }

    private var brandsObserver = Observer<ResultOrError<BrandResults>> {
        brandList = it?.data?.results
        updateCallback()
    }

    private var productsObserver = Observer<ResultOrError<ProductResults>> {
        productList = it?.data?.results
        updateCallback()
    }

    private var brandList: List<Brand>? = null
    private var productList: List<Product>? = null

    fun registerObservers() {
        MochaSdk.Brands.brandResultsLiveData.observeForever(brandsObserver)
        MochaSdk.Products.productResultsLiveData.observeForever(productsObserver)
    }

    fun unregisterObservers() {
        MochaSdk.Brands.brandResultsLiveData.removeObserver(brandsObserver)
        MochaSdk.Products.productResultsLiveData.removeObserver(productsObserver)
    }

    private fun updateCallback() {
        val hasBrands = brandList?.isNotEmpty() ?: false
        val hasProducts = productList?.isNotEmpty() ?: false
        val showWhat: ShowWhat = when {
            !hasBrands && !hasProducts -> ShowWhat.BRANDS
            hasBrands && !hasProducts -> ShowWhat.BRANDS
            hasProducts && !hasBrands -> ShowWhat.PRODUCTS
            productList?.size ?: 0 > 1 -> ShowWhat.PRODUCTS
            else -> ShowWhat.BRANDS
        }
        callback(showWhat)
    }

}