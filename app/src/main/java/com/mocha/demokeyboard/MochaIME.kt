package com.mocha.demokeyboard

import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import com.mocha.demokeyboard.MochaIME.KeyboardState.*
import com.mocha.extensions.UserInputInterceptor
import com.mocha.inputmethod.latin.LatinIME
import com.mocha.sdk.*
import com.mocha.sdk.KeyboardContext.*
import com.mocha.sdk.widgets.MochaBrandsView

/**
 * A custom IME service.
 */
class MochaIME : LatinIME() {

    /**
     * Define various keyboard states. Add your own custom states if necessary.
     */
    private enum class KeyboardState {
        KEYBOARD,
        KEYBOARD_CONTENT,
        BRAND_RESULTS
    }

    private val uiHandler = Handler(Looper.getMainLooper())

    private val state = InternalState()

    /**
     * Intercept user input and feed it into the SDK to get quick link results.
     */
    private val userInputInterceptor = object : UserInputInterceptor() {

        /**
         * Called as the user types text.
         */
        override fun onUserTypingText(text: String) {
            uiHandler.post {
                val sentence = getCurrentSentence()
                Log.d(TAG, "User is typing [$text] in [$sentence]")
                if (state.keyboardContext in SEARCH_CONTEXTS) {
                    MochaSdk.Search.updateSearchText(
                        currentInputEditorInfo,
                        sentence.toString(),
                        SEARCH_WHAT
                    )
                }
            }
        }

        /**
         * Called when the user has finished typing a word or has cleared their text.
         */
        override fun onUserStoppedTyping() {
            uiHandler.post {
                val sentence = getCurrentSentence()
                Log.d(TAG, "User stopped typing. Sentence=[$sentence].")
                MochaSdk.Search.updateSearchText(
                    currentInputEditorInfo,
                    sentence.toString(),
                    SEARCH_WHAT
                )
                updateKeyboardState(KEYBOARD)
            }
        }

    }

    private val searchResultsController = SearchResultsController { showWhat ->
        viewAboveKeyboard?.let {
            when (showWhat) {
                SearchResultsController.ShowWhat.BRANDS -> {
                    it.setQuickLinksViewVisible(true)
                    it.setQuickProductsViewVisible(false)
                }
                SearchResultsController.ShowWhat.PRODUCTS -> {
                    it.setQuickLinksViewVisible(false)
                    it.setQuickProductsViewVisible(true)
                }
            }
        }
    }

    private var viewAboveKeyboard: MochaViewAboveKeyboard? = null

    fun getCurrentSentence(): CharSequence {
        return currentInputConnection?.getTextBeforeCursor(SENTENCE_CHARACTERS, 0) ?: ""
    }

    private val onProductShareClickObserver: Observer<Product> = Observer {
        val text = it.name + " " + it.link
        if (MESSAGING == state.keyboardContext) {
            sendText(text)
        } else {
            shareText(text)
        }
    }

    private val adsObserver = Observer<AdvertResults> {
        Log.d("AdvertsObserver", "value = ${it.adverts.joinToString()}")
    }

    /**
     * Override onCreate() to initialise the SDK and register any observers.
     */
    override fun onCreate() {
        Log.d(TAG, "onCreate")
        super.onCreate()
        setUserInputInterceptor(userInputInterceptor)
        MochaSdk.initialize(this, MochaSdkConfig(campaignId = BuildConfig.CAMPAIGN_ID))
        MochaSdk.QuickLinks.setQuickLinksEnabledInContexts(SEARCH_CONTEXTS)
        MochaSdk.IME.addKeyboardContextRule(CustomBrowserKeyboardContextRule())
        Log.d(TAG, "SDK initialised")
        MochaSdk.Products.onProductShareClickLiveData.observeForever(onProductShareClickObserver)
        searchResultsController.registerObservers()
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy")
        searchResultsController.unregisterObservers()
        MochaSdk.Products.onProductShareClickLiveData.removeObserver(onProductShareClickObserver)
        super.onDestroy()
    }

    /**
     * Override this function to create a view above keyboard spelling suggestions.
     */
    override fun onCreateViewAboveSuggestions(): View? {
        return if (state.keyboardContext == PASSWORD) {
            viewAboveKeyboard = null
            null
        } else
            MochaViewAboveKeyboard(this).also {
                setViewAboveSuggestionsVisible(true)
                viewAboveKeyboard = it
            }
    }

    /**
     * Override this function to destroy the view above keyboard suggestions and free resources.
     */
    override fun onDestroyViewAboveSuggestions() {
        Log.d(TAG, "onDestroyViewAboveSuggestions")
        removeSearchView()
        super.onDestroyViewAboveSuggestions()
    }

    /**
     * Override this method and return true to force display of suggestions
     * (except for password fields).
     */
    override fun forceShowSuggestions() = true

    override fun onStartInput(editorInfo: EditorInfo?, restarting: Boolean) {
        super.onStartInput(editorInfo, restarting)
        if (state.currentEditorInfo.isChanged(editorInfo)) {
            updateKeyboardState(KEYBOARD)
            MochaSdk.Search.resetSearch()
        }
        state.currentEditorInfo = editorInfo
    }

    override fun onStartInputView(editorInfo: EditorInfo?, restarting: Boolean) {
        super.onStartInputView(editorInfo, restarting)
        updateKeyboardContext(MochaSdk.IME.updateEditorInfo(editorInfo))
        MochaSdk.Adverts.advertResultsObservable.observe(adsObserver)
        if (!restarting) {
            // restore view state
            viewAboveKeyboard?.setQuickLinksViewVisible(state.keyboardContext in SEARCH_CONTEXTS)
        }
    }

    override fun onFinishInputView(finishingInput: Boolean) {
        super.onFinishInputView(finishingInput)
        MochaSdk.Adverts.advertResultsObservable.remove(adsObserver)
    }

    override fun onKeyDown(keyCode: Int, keyEvent: KeyEvent?): Boolean {
        if (KeyEvent.KEYCODE_BACK == keyCode && state.keyboardState != KEYBOARD) {
            updateKeyboardState(KEYBOARD)
            return true
        }
        return super.onKeyDown(keyCode, keyEvent)
    }

    override fun showKeyboardContent(view: View) {
        super.showKeyboardContent(view)
        updateKeyboardState(KEYBOARD_CONTENT)
    }

    private fun updateKeyboardContext(keyboardContext: KeyboardContext) {
        state.keyboardContext = keyboardContext
        Log.d(TAG, "keyboard context=${state.keyboardContext}")
        setViewAboveSuggestionsVisible(keyboardContext != PASSWORD)
    }

    private fun updateKeyboardState(state: KeyboardState) {
        if (this.state.keyboardState == state) return
        this.state.keyboardState = state
        when (this.state.keyboardState) {
            KEYBOARD -> hideKeyboardContent()
            BRAND_RESULTS -> showMochaBrandsView()
            else -> {
            }
        }
    }

    private fun removeSearchView() {
        setSearchView(null)
        setShowSearchImeAction(false)
    }

    private fun showMochaBrandsView() {
        if (keyboardContent == null || keyboardContent !is MochaBrandsView) {
            showKeyboardContent(MochaBrandsView(this))
        }
    }

    private fun sendText(text: String) {
        currentInputConnection.run {
            commitText(text, 0)
            performEditorAction(EditorInfo.IME_ACTION_SEND)
        }
        hideSearchView()
    }

    private fun shareText(text: String) {
        Intent(Intent.ACTION_SEND).apply {
            putExtra(Intent.EXTRA_TEXT, text)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            type = MIME_TYPE_PLAIN_TEXT
            startActivity(this)
        }
        hideSearchView()
    }

    private fun hideSearchView() {
        removeSearchView()
        updateKeyboardState(KEYBOARD)
    }

    private companion object {

        private val TAG = MochaIME::class.java.simpleName
        private val SEARCH_CONTEXTS = setOf(MESSAGING, BROWSER)
        private const val MIME_TYPE_PLAIN_TEXT = "text/plain"
        private const val SENTENCE_CHARACTERS = 300
        private val SEARCH_WHAT = setOf(
            MochaSdk.Search.SearchWhat.BRANDS, MochaSdk.Search.SearchWhat.PRODUCTS
        )

    }

    private class InternalState {

        var keyboardContext: KeyboardContext = NONE
        var keyboardState = KEYBOARD

        var currentEditorInfo: EditorInfo? = null

    }

}